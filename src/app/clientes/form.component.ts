import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import {ClienteService} from "./cliente.service";
import { Router, ActivatedRoute } from "@angular/router";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  public cliente: Cliente = new Cliente();
  titulo: string = "Crear cliente";
  public errores: string[];
  private clienteService: ClienteService;
  private router: Router;
  private activateRouter: ActivatedRoute;

  constructor( clienteService:ClienteService, router: Router, activateRouter: ActivatedRoute ) {
    this.clienteService = clienteService;
    this.router = router;
    this.activateRouter = activateRouter;
  }

  ngOnInit(): void {
    this.cargarCliente();
  }

  cargarCliente():void {
    this.activateRouter.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.clienteService.getCliente(id).subscribe(
          (clienteResponse) => this.cliente = clienteResponse );
      }
    })
  }

  public create():void {

    this.clienteService.create(this.cliente).subscribe({
        next: (response) => {
          this.router.navigate(['/clientes']);
          console.log(response);
          Swal.fire({
            title: 'Nuevo cliente',
            text:  `Cliente ${response.cliente.nombre} creado con exito!`,
            icon:  'success'
          });
        },
        error: (error) => {
          this.errores = error.error.errors as string[];
          console.error(`Codigo del error desde el backend: ${error.error.errors}`);
          console.error(error.error.errors);
        }
      }
    );
  }

  update():void {
    this.clienteService.update(this.cliente).subscribe(response => {
      this.router.navigate(['/clientes']);
      Swal.fire({
        title: 'Cliente actualizado',
        text:  `Cliente ${response.cliente.nombre} actualizado con exito!`,
        icon:  'success'
      });
    });
  }

}
