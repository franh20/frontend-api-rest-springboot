import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
  {
    id: 1,
    nombre: 'Franklin',
    apellido: 'Huichi',
    email: 'carloshuichi@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 2,
    nombre: 'Yofer',
    apellido: 'Farfan',
    email: 'yofarfan@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 3,
    nombre: 'Katherine',
    apellido: 'Estrella',
    email: 'kaestrella@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 4,
    nombre: 'Marco',
    apellido: 'Polo',
    email: 'mapolo@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 5,
    nombre: 'Jose',
    apellido: 'Ochoa',
    email: 'jochoa@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 6,
    nombre: 'Natalia',
    apellido: 'Huichi',
    email: 'jazminhuichi@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 7,
    nombre: 'Manuel',
    apellido: 'Manzaneda',
    email: 'mamanzaneda@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 8,
    nombre: 'Ericka',
    apellido: 'Perez',
    email: 'erperez@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 9,
    nombre: 'Paola',
    apellido: 'Bustamente',
    email: 'pabustamente@gmail.com',
    createAt: '2021-02-16',
  },
  {
    id: 10,
    nombre: 'Francisco',
    apellido: 'Paez',
    email: 'fapaez@gmail.com',
    createAt: '2021-02-16',
  },
];
