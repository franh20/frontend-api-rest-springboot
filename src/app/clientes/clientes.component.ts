import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { ActivatedRoute } from '@angular/router';

import Swal from "sweetalert2";

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  private clientesService: ClienteService;
  private activatedRoute: ActivatedRoute;

  constructor( clientesService: ClienteService, activatedRoute: ActivatedRoute ) {
    this.clientesService = clientesService;
    this.activatedRoute = activatedRoute;
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe( params => {
      let page:number = +params.get("page");
      if(!page) page = 0;
      this.clientesService.getClientes(page).subscribe(
        (response) => this.clientes = response.content as Cliente[]
      );
    });
  }

  delete(cliente:Cliente): void {

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: `¿Estas seguro de eliminar al cliente ${cliente.nombre} ${cliente.apellido}?`,
      text: "!No vas a poder revertirlo!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quiero borrarlo!',
      cancelButtonText: 'No, cancelalo!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.clientes = this.clientes.filter(cli => cli !== cliente);
        this.clientesService.delete(cliente.id).subscribe( response => {
          swalWithBootstrapButtons.fire(
            'Borrado!',
            'Tu registro fue borrado.',
            'success'
          )
        });

      } else if (
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No fue borrado :)',
          'error'
        )
      }
    });

  }

}
